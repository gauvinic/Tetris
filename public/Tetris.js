var isRunning = false;
var canScatter = true;

var currentState = [];
var nextState = [];
var cellColors = [];
var newCellColors = [];
var cellElements = [];

var previewerCells = [];

var board = document.getElementsByClassName("board")[0];
var previewer = document.getElementsByClassName("previewer")[0];
var highScoreDisplay = document.getElementById("high-score-display");
var scoreDisplay = document.getElementById("score-display");
var levelDisplay = document.getElementById("level-display");
var playButton = document.getElementById("play-button");

var GRID_HEIGHT = 20;
var GRID_WIDTH = 10;
var GAME_SCORE = 0;
var HIGH_SCORE = 0;
var MULTIPLIER = 1;
var LEVEL = 1;
var GAME = false;
var GAME_AUDIO = new Audio('Tetris.mp3');
GAME_AUDIO.loop = true;

var I = [ 0,0,0,0,
          2,1,1,1 ];

var J = [ 1,0,0,0,
          2,1,1,0 ];

var L = [ 0,0,1,0,
          1,1,2,0 ];

var O = [ 1,1,0,0,
          2,1,0,0 ];

var S = [ 0,1,1,0,
          1,2,0,0 ];
  
var T = [ 0,1,0,0,
          1,2,1,0 ];

var Z = [ 1,1,0,0,
          0,2,1,0 ]; // 0 = empty
                     // 1 = rotatable
                     // 2 = origin
                     // 3 = locked (only used on blocks that have fallen to the bottom)

var pieces = [ I, J, L, O, S, T, Z ];
var pieceColors = [ 'blue', 'orange', 'green', 'magenta', 'cyan', 'yellow', 'red' ]; // from Super Tetris 3 (Super Famicom) (SNES)

highScoreDisplay.innerHTML = "High Score: " + HIGH_SCORE.toString();

init();

function init()
{
    setupGrid();
    setBackingFields();
}

function setupGrid()
{
    var previewerContainer = document.getElementsByClassName("previewer-container")[0];
    previewerContainer.style.minHeight = 2*26.75;
    previewerContainer.style.minWidth = 4*26.75;
    previewerContainer.style.height = 2*26.75;
    previewerContainer.style.width = 4*26.75;
    previewerContainer.style.maxHeight = 2*26.75;
    previewerContainer.style.maxWidth = 4*26.75;

    previewer.style.minWidth = 4*26.75;
    previewer.style.minHeight = 2*26.75;
    previewer.style.width = 4*26.75;
    previewer.style.height = 2*26.75;
    previewer.style.maxWidth = 4*26.75;
    previewer.style.maxHeight = 2*26.75;
    
    board.style.setProperty("grid-template-columns", `repeat(${Math.floor(GRID_WIDTH)}, 1fr)`);
    board.style.setProperty("grid-template-rows", `repeat(${Math.floor(GRID_HEIGHT)}, 1fr)`);

    previewer.style.setProperty("grid-template-columns", `repeat(${Math.floor(4)}, 1fr)`);
    previewer.style.setProperty("grid-template-rows", `repeat(${Math.floor(2)}, 1fr)`);

    levelDisplay.innerHTML = "Level: " + LEVEL.toString();
    scoreDisplay.innerHTML = "Score: " + GAME_SCORE.toString();

    for(var i = 0; i < GRID_WIDTH*GRID_HEIGHT; i++) nextState[i] = 0;
    
    for(var i = 0; i < GRID_WIDTH*GRID_HEIGHT; i++)
    {
        var element = document.createElement("div");
        element.dataset.cellNumber = i;
        element.classList.add("cell");
        
        cellElements[i] = element;
        board.appendChild(element);
    }

    for(var i = 0; i < 8; i++)
    {
        var element = document.createElement("div");
        element.classList.add("cell");

        previewerCells[i] = element;
        previewer.appendChild(element);
    }
}

function setBackingFields()
{
    for(var i = 0; i < GRID_WIDTH*(GRID_HEIGHT + 5); i++) // give room for hidden blocks up top
    {
        currentState[i] = 0;
        nextState[i] = 0;
        cellColors[i] = 'transparent';
    }
}

function modifyCSS(className, element, value)
{
    if(!document.getElementById('customStyleSheet'))
    {
        var css = 'div.cell:hover { background: #FFFFFF; }'
        var style = document.createElement('style');

        style.id = 'customStyleSheet';

        if(style.styleSheet)
            style.styleSheet.cssText = css;
        else
            style.appendChild(document.createTextNode(css));

        document.getElementsByTagName('head')[0].appendChild(style);
    }

    var cssRuleCode = document.all ? 'rules' : 'cssRules';
    
    for(var i = 0; i < document.styleSheets.length; i++)
    {
        if(document.styleSheets[i][cssRuleCode] != 'transparent')
        {
            for(var j = 0; j < document.styleSheets[i][cssRuleCode].length; j++)
            {
                if(document.styleSheets[i][cssRuleCode][j].selectorText == className && 
                   document.styleSheets[i][cssRuleCode][j].style[element])
                {
                    document.styleSheets[i][cssRuleCode][j].style[element] = value;
                    return;
                }
            
            }
        }
    }

    console.log('Unable to find ' + className + ' with element ' + element + ' in style sheets. Value was not set to ' + value + '.');
}

function reset()
{
    clearInterval(GAME);

    GAME = false;

    playButton.disabled = false;

    GAME_AUDIO.pause();
    GAME_AUDIO.currentTime = 0;
    GAME_AUDIO.playbackRate = 1;

    LEVEL = 1;
    GAME_SCORE = 0;

    levelDisplay.innerHTML = "Level: " + LEVEL.toString();
    scoreDisplay.innerHTML = "Score: " + GAME_SCORE.toString();

    for(var i = 0; i < currentState.length; i++)
    {
        currentState[i] = 0;
        nextState[i] = 0;
        cellColors[i] = 'transparent';
        newCellColors[i] = 'transparent';
    }

    updateBoard();

    for(var i = 0; i < previewerCells.length; i++)
        previewerCells[i].style.backgroundColor = 'transparent';
}

function setCell(cellNumber, color)
{
    if(cellNumber < 0 || cellNumber >= cellElements.length) return;
    cellElements[cellNumber].style.backgroundColor = color;
}


function updateBoard()
{
    var offset = 5*GRID_WIDTH;
    for(var i = offset; i < currentState.length; i++)
    {
        setCell(i - offset, cellColors[i]);
    }
}

function getScore()
{
    var completedRows = [];
    for(var i = GRID_HEIGHT + 4; i > 4; i--)
    {
        var completedRow = true;
        for(var j = 0; j < GRID_WIDTH; j++)
        {
            if(currentState[GRID_WIDTH*i + j] != 3) // cell not in locked state, so row is not complete
            {
                completedRow = false;
                break;
            }
        }
        if(completedRow) // all cells in row were in lock state
            completedRows.push(i);
    }

    if(completedRows.length > 0) // if any rows were complete
    {
        var score = Math.pow(2, completedRows.length - 1) * 100; // clearing 1 row:  100 pts, 
                                                                 //          2 rows: 200 pts,
                                                                 //          3 rows: 400 pts,
                                                                 //          4 rows: 800 pts
        
        var downShift = 1; // variable to shift above cells down by
        while(completedRows.length > 0) // while we haven't touched all completed rows
        {
            var rowToRemove = completedRows.shift(); // get the first completed row untouched (will be in descending order by row number)
            var stopAtRow = completedRows[0]; // get the row to stop before
            if(stopAtRow === undefined)
                stopAtRow = -1; // if there are no more completed rows, go all the way up to the top.

            for(var i = 0; i < GRID_WIDTH; i++) // remove row
            {
                currentState[GRID_WIDTH*rowToRemove + i] = 0;
                cellColors[GRID_WIDTH*rowToRemove + i] = 'transparent';
            }

            for(var i = GRID_WIDTH*rowToRemove - 1; i > GRID_WIDTH*stopAtRow + GRID_WIDTH; i--) // for all cells under next row to remove
            {
                currentState[i + GRID_WIDTH*downShift] = currentState[i]; // shift them down by downShift
                cellColors[i + GRID_WIDTH*downShift] = cellColors[i];

                currentState[i] = 0; // clear them
                cellColors[i] = 'transparent';
            }

            downShift++; // increment downShift for the next set of cells to shift down.
        }

        updateBoard(); // display results

        return score; // return the score the player earned
    }

    return 0; // no rows to remove, so player did not score anything additional
}

function updateScore(additional)
{
    GAME_SCORE += Math.floor(additional * MULTIPLIER);
    scoreDisplay.innerHTML = "Score: " + GAME_SCORE.toString();

    if(GAME_SCORE > HIGH_SCORE)
    {
        HIGH_SCORE = GAME_SCORE;
        highScoreDisplay.innerHTML = "High Score: " + HIGH_SCORE.toString();
    }

    if(GAME_SCORE >= Math.pow(2, LEVEL))
    {
        nextLevel(); // increment and show the new level player has attained

        clearInterval(GAME); // stop running the game

        GAME = setInterval(drop, GAME_SPEED); // run the game with the new speed
    }
}

function randomInt(min, max)
{
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

function scatter()
{
    if(canScatter)
    {
        var lockedBlocks = 0;
        var lockedColors = [ 0, 0, 0, 0, 0, 0, 0 ];
        var highestRow = 25;

        for(var i = 0; i < currentState.length; i++)
        {
            if(currentState[i] == 3) // locked
            {
                currentState[i] = 0;
                lockedBlocks++;

                for(var j = 0; j < pieceColors.length; j++)
                {
                    if(cellColors[i] == pieceColors[j])
                    {
                        lockedColors[j]++;
                        cellColors[i] = 'transparent';
                        break;
                    }
                }

                if(highestRow > Math.floor(i/10))
                    highestRow = Math.floor(i/10)
            }
        }

        var colorIndex = 0;
        for(var i = 0; i < lockedBlocks; i++)
        {
            while(lockedColors[colorIndex] == 0)
                colorIndex++;
            if(colorIndex == 7)
                break;

            var index = randomInt(10*highestRow, currentState.length);
            while(currentState[index] != 0)
                index = randomInt(10*highestRow, currentState.length);

            currentState[index] = 3;
            cellColors[index] = pieceColors[colorIndex];
            lockedColors[colorIndex]--;
        }

        updateBoard();

        canScatter = false;
    }
}

function nextLevel()
{
    LEVEL++;
    levelDisplay.innerHTML = "Level: " + LEVEL.toString();

    MULTIPLIER += .1; // points now are worth more

    GAME_AUDIO.playbackRate *= 1.05; // speed up audio

    GAME_SPEED = Math.floor(GAME_SPEED/1.1); // increase the game speed (decrease the time between forced drops)
}

function setGameSpeed(speed)
{
    GAME_SPEED = Math.floor(speed);

    GAME = setInterval(drop, GAME_SPEED);
}

function drop()
{
    for(var i = 0; i < nextState.length; i++)
    {
        nextState[i] = currentState[i];
        newCellColors[i] = cellColors[i];
    }

    var additional = getScore();
    if(additional != 0)
    {
        updateScore(additional);
        return;
    }

    var needNewPiece = true;
    var needToMove = false;
    for(var i = 0; i < currentState.length; i++)
    {
        if(currentState[i] == 1 || currentState[i] == 2) // if block should move or be locked
        {
            if(i + GRID_WIDTH < nextState.length && currentState[i + GRID_WIDTH] != 3) // if block can move down
            {
                nextState[i + GRID_WIDTH] = currentState[i]; // move block
                newCellColors[i + GRID_WIDTH] = cellColors[i];

                if(currentState[i - GRID_WIDTH] == 0 || currentState[i - GRID_WIDTH] == 3)
                {
                    nextState[i] = 0;
                    newCellColors[i] = 'transparent';
                }

                needToMove = true;
            }
            else // abort and lock down all blocks
            {
                needToMove = false;
                break;
            }

            needNewPiece = false;
        }
    }

    if(needToMove)
        for(var i = 0; i < currentState.length; i++) // move next state to current state
        {
            currentState[i] = nextState[i];
            cellColors[i] = newCellColors[i];
        }
    else
        for(var i = 0; i < currentState.length; i++) // lock all blocks
            currentState[i] = currentState[i] == 0 ? 0 : 3;

    if(didPlayerLose()) // check for lost state
    {
        reset();
        return;
    }

    if(needNewPiece) // place next piece on board, and generate new next piece
    {
        placeOnBoard(NEXT_PIECE_INDEX);
        nextPiece = generateNewPiece();
        canScatter = true;
        updatePreviewer();
    }

    updateBoard(); // show the change
}

function placeOnBoard(pieceIndex)
{
    var transpose = 43;
    for(var i = 0; i < pieces[pieceIndex].length; i++)
    {
        if(i == 4)
            transpose += 6;

        currentState[i + transpose] = pieces[pieceIndex][i];
        cellColors[i + transpose] = pieces[pieceIndex][i] != 0 ? pieceColors[pieceIndex] : 'transparent';
    }
}

function generateNewPiece()
{
    NEXT_PIECE_INDEX = Math.floor(Math.random() * 1000) % pieces.length;
}

function updatePreviewer()
{
    for(var i = 0; i < previewerCells.length; i++)
    {
        previewerCells[i].style.backgroundColor = pieces[NEXT_PIECE_INDEX][i] == 0 ? 'transparent' : pieceColors[NEXT_PIECE_INDEX];
    }
}

function didPlayerLose()
{
    for(var i = 0; i < 5*GRID_WIDTH; i++)
    {
        if(currentState[i] == 3) // locked block out of view
            return true; // thus, player loses
    }

    return false; // no locked blocks out of view, so player hasn't lost yet
}

function startNewGame()
{
    playButton.disabled = true;

    setBackingFields();
    updateBoard();
    updateScore(0);
    levelDisplay.innerHTML = "Level: " + LEVEL.toString();

    generateNewPiece();
    placeOnBoard(NEXT_PIECE_INDEX);
    generateNewPiece();
    updatePreviewer();
    updateBoard();

    setGameSpeed(1000);

    GAME_AUDIO.currentTime = 0;
    GAME_AUDIO.playbackRate = 1;
    GAME_AUDIO.play();
}

function shiftLeft()
{
    var newState = [];
    var newColors = [];

    var validMove = true;
    for(var i = 0; i < currentState.length; i++)
    {
        newState[i] = currentState[i];
        newColors[i] = cellColors[i];

        if(i % 10 != 0) // if not leftmost
        {
            if(currentState[i] != 3) // if not locked
            {
                if((currentState[i-1] == 3) && (currentState[i] != 0))
                {   
                    validMove = false;
                    break;
                }

                newState[i-1] = currentState[i];
                newColors[i-1] = cellColors[i];
            }
        }
        else if(currentState[i] == 1 ||
                currentState[i] == 2)
        {
            validMove = false;
            break;
        }

        if(i % 10 != 9 && currentState[i+1] == 3)
        {
            newState[i] = 0;
            newColors[i] = 'transparent';
        }
        
        if(i % 10 == 9 &&
           currentState[i] != 3)
        {
            newState[i] = 0;
            newColors[i] = 'transparent';
        }
    }

    if(validMove)
    {
        for(var i = 0; i < currentState.length; i++)
        {
            if(currentState[i] != 3)
            {
                currentState[i] = newState[i];
                cellColors[i] = newColors[i];
            }
        }

        updateBoard();
    }
}

function shiftRight()
{
    var newState = [];
    var newColors = [];

    var validMove = true;
    for(var i = currentState.length - 1; i >= 0; i--)
    {
        newState[i] = currentState[i];
        newColors[i] = cellColors[i];

        if(i % 10 != 9) // if not rightmost
        {
            if(currentState[i] != 3) // if not locked
            {
                if((currentState[i+1] == 3) && (currentState[i] != 0))
                {   
                    validMove = false;
                    break;
                }

                newState[i+1] = currentState[i];
                newColors[i+1] = cellColors[i];
            }
        }
        else if(currentState[i] == 1 ||
                currentState[i] == 2)
        {
            validMove = false;
            break;
        }

        if(i % 10 != 0 && currentState[i-1] == 3)
        {
            newState[i] = 0;
            newColors[i] = 'transparent';
        }
        
        if(i % 10 == 0 &&
           currentState[i] != 3)
        {
            newState[i] = 0;
            newColors[i] = 'transparent';
        }
    }

    if(validMove)
    {
        for(var i = 0; i < currentState.length; i++)
        {
            if(currentState[i] != 3)
            {
                currentState[i] = newState[i];
                cellColors[i] = newColors[i];
            }
        }

        updateBoard();
    }
}

function rotateLeft()
{
    var newState = [];
    var newColor = [];

    var XOrigin = 0;
    var YOrigin = 0;

    var X = [];
    var Y = [];

    var XPrime = [];
    var YPrime = [];

    for(var i = 0; i < currentState.length; i++) // find origin and store rotatable points
    {
        newState[i] = 0;
        newColor[i] = 'transparent';
        if(currentState[i] == 1)
        {
            X.push(i % 10);
            Y.push(Math.floor(i / 10));
        }
        else if(currentState[i] == 2)
        {
            XOrigin = i % 10;
            YOrigin = Math.floor(i / 10);

            newState[i] = currentState[i];
            newColor[i] = cellColors[i];
        }
        else if(currentState[i] == 3)
        {
            newState[i] = 3;
            newColor[i] = cellColors[i];
        }
    }

    var paddingLeft = Math.abs(YOrigin - (min(Y) > YOrigin ? YOrigin : min(Y)));
    var paddingBelow = Math.abs(XOrigin - (min(X) > XOrigin ? XOrigin : min(X)));
    var paddingRight = Math.abs(YOrigin - (max(Y) < YOrigin ? YOrigin : max(Y)));

    if(XOrigin - paddingLeft < 0 ||
       YOrigin + paddingBelow > GRID_HEIGHT + 5 ||
       XOrigin + paddingRight >= GRID_WIDTH)
        return;

    for(var i = 0; i < X.length; i++) // compute transposed points
    {
        X[i] -= XOrigin;
        Y[i] -= YOrigin;

        Y[i] *= -1;

        var xprime = -1*Y[i];
        var yprime = X[i];

        yprime *= -1;

        xprime += XOrigin;
        yprime += YOrigin;

        if(currentState[yprime*10 + xprime] == 3)
            return;

        XPrime.push(xprime);
        YPrime.push(yprime);
    }

    var rotatingColor = cellColors[YOrigin*10 + XOrigin];

    for(var i = 0; i < XPrime.length; i++)
    {
        newState[YPrime[i]*10 + XPrime[i]] = 1;
        newColor[YPrime[i]*10 + XPrime[i]] = rotatingColor;
    }

    for(var i = 0; i < currentState.length; i++)
    {
        currentState[i] = newState[i];
        cellColors[i] = newColor[i];
    }

    updateBoard();
}

function min(values)
{
    var minVal = 1000;

    for(var i = 0; i < values.length; i++)
    {
        if(values[i] < minVal)
            minVal = values[i];
    }

    return minVal;
}

function max(values)
{
    var maxVal = -1000;

    for(var i = 0; i < values.length; i++)
    {
        if(values[i] > maxVal)
            maxVal = values[i];
    }

    return maxVal;
}

function rotateRight()
{
    var newState = [];
    var newColor = [];

    var XOrigin = 0;
    var YOrigin = 0;

    var X = [];
    var Y = [];

    var XPrime = [];
    var YPrime = [];

    for(var i = 0; i < currentState.length; i++) // find origin and store rotatable points
    {
        newState[i] = 0;
        newColor[i] = 'transparent';
        if(currentState[i] == 1)
        {
            X.push(i % 10);
            Y.push(Math.floor(i / 10));
        }
        else if(currentState[i] == 2)
        {
            XOrigin = i % 10;
            YOrigin = Math.floor(i / 10);

            newState[i] = currentState[i];
            newColor[i] = cellColors[i];
        }
        else if(currentState[i] == 3)
        {
            newState[i] = 3;
            newColor[i] = cellColors[i];
        }
    }

    var paddingLeft = Math.abs(YOrigin - (max(Y) < YOrigin ? YOrigin : max(Y)));
    var paddingBelow = Math.abs(XOrigin - (max(X) < XOrigin ? XOrigin : max(X)));
    var paddingRight = Math.abs(YOrigin - (min(Y) > YOrigin ? YOrigin : min(Y)));

    if(XOrigin - paddingLeft < 0 ||
       YOrigin + paddingBelow > GRID_HEIGHT + 5 ||
       XOrigin + paddingRight >= GRID_WIDTH)
        return;

    for(var i = 0; i < X.length; i++) // compute transposed points
    {
        X[i] -= XOrigin;
        Y[i] -= YOrigin;

        Y[i] *= -1;

        var xprime = Y[i];
        var yprime = -1*X[i];

        yprime *= -1;

        xprime += XOrigin;
        yprime += YOrigin;

        if(currentState[yprime*10 + xprime] == 3)
            return;

        XPrime.push(xprime);
        YPrime.push(yprime);
    }

    var rotatingColor = cellColors[YOrigin*10 + XOrigin];

    for(var i = 0; i < XPrime.length; i++)
    {
        newState[YPrime[i]*10 + XPrime[i]] = 1;
        newColor[YPrime[i]*10 + XPrime[i]] = rotatingColor;
    }

    for(var i = 0; i < currentState.length; i++)
    {
        currentState[i] = newState[i];
        cellColors[i] = newColor[i];
    }

    updateBoard();
}

document.getElementsByTagName('html')[0].addEventListener("keydown", function(event)
{
    console.log(event.which);
    switch(event.which)
    {
        case 32: // spacebar
            drop();
            break;
        case 37: // left arrow
            shiftLeft();
            break;
        case 38: // up arrow
            rotateLeft();
            break;
        case 39: // right arrow
            shiftRight();
            break;
        case 40: // down arrow
            rotateRight();
            break;
        case 83: // s
            scatter();
            break;
        default: 
            console.log(event.which);
            break;
    }
});
